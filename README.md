# Behat inside Docker

## Running the tests

1 - Set credentials for the AWS SDK for PHP
(https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials.html)

2 - Set S3 bucket name in behat.yml

3 - Run composer update
``` bash
$ composer update
```

4 - Run docker container
``` bash
$ composer selenium-chrome
#OR
$ composer selenium-firefox
```

5 - Run tests
``` bash
$ composer test
```

##

Docker images can be found at https://github.com/SeleniumHQ/docker-selenium 
