<?php
namespace App\Tests;

use App\Tests\Helpers\BehatUtility;
use Behat\MinkExtension\Context\MinkContext;


/**
 * UtilityContext is used for any function which is irrelevant of code base.
 */
class UtilityContext extends MinkContext {

	/**
	 * @BeforeStep
	 */
	public function beforeStep()
	{
		try {
			$this->getSession()->maximizeWindow('current');
			$this->getSession()->setCookie('X-IS-SELENIUM', true);
		} catch(\Exception $e) {
			// do nothing - unsupported driver issue
		}
	}

    /**
     * @When /^I resize the window to desktop$/
     */
    public function resizeTheWindowToDesktop()
    {
        $this->getSession()->resizeWindow(1440, 900, 'current');
    }

	/**
     * @Given /^wait for "([^"]*)" second[s]*$/
     * @param string $seconds Seconds
	 */
	public function waitForSeconds($seconds) {

		$this->getSession()->getPage()->waitFor($seconds, function() { });
	}

	/**
	 * @Given /^I refresh|reload the page$/
	 */
	public function iRefreshThePage() {
		$this->getSession()->reload();
	}

	/**
	 * @Given /^the response header "([^"]*)" should be set$/
	 */
	public function theResponseHeaderShouldBeSet($header) {
		return !empty($this->getSession()->getResponseHeader($header));
	}

	/**
	 * @Then /^I should receive a redirect$/
	 */
	public function iShouldReceiveARedirect() {
		return $this->getSession()->getStatusCode() == 302 && !empty($headers[$this->getSession()->getResponseHeader('Location')]);
	}

	/**
	 * @Given /^I click on element with selector "([^"]*)"$/
	 */
	public function iClickWithSelector( $selector ) {
		$link = $this->getSession()->getPage()->find('css', $selector);
		$link->click();
	}

	/**
	 * @Then /^I should wait and see "([^"]*)"$/
	 */
	public function iShouldWaitAndSee( $text ) {

		BehatUtility::spins(function() use ($text) {
			// wil throw an exception if we can't see it
			$this->assertSession()->pageTextContains($text);
			// only if no exception is thrown
			return true;
		});
	}

    /**
     * @Then /^I click on element with ID "([^"]*)"$/
     * @param string $elementId Element ID
     * @throws \Exception
     */
    public function iClickOnElementWithId($elementId)
    {
        $page = $this->getSession()->getPage();
        $findName = $page->findById($elementId);
        if (!$findName) {
            throw new \Exception($elementId . " could not be found");
        } else {
            $findName->click();
        }
    }

}
