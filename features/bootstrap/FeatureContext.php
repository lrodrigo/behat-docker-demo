<?php

namespace App\Tests;

use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Mink\Driver\Selenium2Driver;
use Behat\Behat\Hook\Scope\AfterStepScope;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawMinkContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Then /^I should wait and see "([^"]*)"$/
     * @param $text string
     */
    public function iShouldWaitAndSee( $text ) {

        FeatureContext::spins(function() use ($text) {
            // wil throw an exception if we can't see it
            $this->assertSession()->pageTextContains($text);
            // only if no exception is thrown
            return true;
        });
    }

    /**
     * @AfterStep
     * @param $scope
     */
    public function takeScreenShotAfterFailedStep(AfterStepScope $scope)
    {
        /*if (99 === $scope->getTestResult()->getResultCode()) {
            $driver = $this->getSession()->getDriver();
            if (!($driver instanceof Selenium2Driver)) {
                return;
            }
            file_put_contents('/tmp/test' . time() . '.png', $this->getSession()->getDriver()->getScreenshot());
        }*/
    }

    public static function spins(callable $closure, $wait = 45, $step = 250000)
    {
        $error     = null;
        $stop_time = time() + $wait;

        while (time() < $stop_time) {
            try {
                $response = call_user_func($closure);
                if ($response) {
                    return;
                }
            } catch (\Exception $e) {
                $error = $e;
            }

            usleep($step);
        }

        throw $error;
    }
}
