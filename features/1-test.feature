@test
Feature: Test feature
  Check that behat works

  @javascript
  Scenario: Behat works
    Given I am on "http://behat.org/en/latest/"
    When I follow "Guides"
    Then I should see "Behat Documentation"

  @javascript
  Scenario: Open modal dialog
    Given I am on "http://getbootstrap.com/2.3.2/javascript.html#modals"
    And I should see "Launch demo modal"
    When I follow "Launch demo modal"
    Then I should wait and see "Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem."

  @javascript
  Scenario: Failing test
    Given I am on "http://behat.org/en/latest/"
    When I follow "Guides"
    Then I should see "asdasdasd"