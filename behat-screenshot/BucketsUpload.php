<?php
/**
 * Created by PhpStorm.
 * User: lrodrigo
 * Date: 02/01/18
 * Time: 19:10
 */

namespace Bex\Behat\ScreenshotExtension\Driver;

use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Aws\S3\S3Client;

class BucketsUpload implements ImageDriverInterface
{

    const CONFIG_PARAM_BUCKET = 'bucket';
    const CONFIG_PARAM_ACL = 'ACL';

    private $bucket;

    private $acl;

    /**
     * @param \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $builder
     * @return mixed
     */
    public function configure(ArrayNodeDefinition $builder)
    {
        $builder
            ->children()
                ->scalarNode(self::CONFIG_PARAM_BUCKET)
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->end()
                ->enumNode(self::CONFIG_PARAM_ACL)
                    ->values(array('private','public-read','public-read-write','authenticated-read','aws-exec-read','bucket-owner-read','bucket-owner-full-control'))
                    ->defaultValue('private')
                    ->end();
    }

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     * @param array $config
     * @return mixed
     */
    public function load(ContainerBuilder $container, array $config)
    {
        $this->bucket = $this->resolveBucketParam($config[self::CONFIG_PARAM_BUCKET]);
        $this->acl = $config[self::CONFIG_PARAM_ACL];
    }

    /**
     * @param string $binaryImage
     * @param string $filename
     * @return mixed
     */
    public function upload($binaryImage, $filename)
    {
        return $this->doUpload($binaryImage, $filename);
    }

    private function doUpload($binaryImage, $filename)
    {
        // Instantiate the client.
        $s3 = S3Client::factory();

        try {
            // Upload a file.
            $result = $s3->putObject(array(
                'ACL' => $this->acl,
                'Bucket' => $this->bucket,
                'Key' => $filename,
                'Body' => $binaryImage,
                'ContentType' => 'image/png'
            ));

            return $result['ObjectURL'];

        } catch (S3Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    private function resolveBucketParam($bucketParam)
    {
        if (!empty($bucketParam)) {
            return $bucketParam;
        } else {
            // Default bucket name
        }
    }

}